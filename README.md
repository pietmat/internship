# Internship

A place for all of the documents, info, notes and reports about my internship at Itelligence.

# GitHub.

I am uploading all of the code I am working with here. You're welcome to look through my [GitHub.](https://github.com/pietmat/InternshipWork)
