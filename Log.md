## 11.01.2021

**Introduction meeting together with Ole from Itelligence.**

-  Introduction+Specifying my task
-  Python Programming. **Creating a GUI for face recognition system**.

## 12.01.2021

-   Brief look at front0end code of face reecognition preojcet.
-   Meeting with Steven, one of the technicians from Itelligence.
-   I have to research on RabbitMQ.
## 13.01.2021

-   I am practicing Python with a focus on GUIs. I have started by looking and using **OpenCV** for python.
-   I have also started a RabbitMQ tutorial, however I am having issues with it, code needs debugging, maybe I have to install additional packages to make the first step work.
-   Meeting with Niels, Itelligences' dev, tommorow at 12.

## 14.01.2021
-   Meeting with Niels and Steven. We talked about the idea and concept.
-   I saw the outout from the 3D camera we're using for this project.
-   Programming exercises, continuing on practicing OpenCV.

## 15.01.2021
-   I am Creating a flowchart of my task based on information I got so far.
-   I am in contact with my mentor, Ole. We are exchanging ideas.

## 18.01.2021
-   Programming exercises, continuing OpenCV course.
-   Creating flowcharts with different scenarios about opening doors.
-   different user cases, adding features ideas into project.

## 19-22.01.2021

-   Generally programming exercises, leaning about machine learning, OPENCV andn PySimpleGUI.
-   Creating UI designs for different use case scenarios.
-   Setting up a meeting for next week to pick up a task.
##  25.01.2021

-   Visit in Itelligences office in Horsens
-   Get-to-know the environment of the company.
-   I got my gear - laptop and a phone, for company-intern use cases only

## 26.01.2021 - 29.01.2021

-   Mainly python programming.
-   Setting up the environment for sso face recognition project. 
-   Consultation with Niels and Steven


## 1.02.2021 - 5.02.2021

-   This week I have started on HTML, CSS and JavaScript. 
-   pure python-GUI programming is quite limited, therefore I must switch and add other languages to this project.

## 8.02.2021 - 12.02.2021 

-   Bootstrap. I have started using Bootstrap and I am fairly enjoying it. It provides a wide range of customization for designs and is well documented.
-   Buttons for second screen. I have started on adjusting and re-designing them.


## week 7

- continuing on developing the project.Precisely speaking,  debugging frontend file as well as the image publisher python file. Both were in need of tweaking which was succesfully done with a help from Steven, my co-worker. 

## week 8 22.02-28.02

- Several meetings with UX intern at Itelligence. We are going to consult and redesign welcome screen as well as the video feed screen. Lenette is using Axure and gives me advices and feedback from her perspective, which I find veryh useful.

## week 9 01.03-07.03

Another week of updating and changing UI. This week I have focused on background and its properties. Other elements of I worked around this week were JavaScript elements for modals and also navbar adjustment.

## week 10 08.03-14.03

- Monday - meeting with Lenette, we have worked with adjusting HTML semantics and talked about CSS - inline/internal/external. 

- Tuesday - Meeting with Steven, we have debugged Flask elements of frontend code. Video feed had to be adjusted and moved into other window, therefore we had to change frontend.py a bit. 

- Thursday - I had a feedback meeting together with Ole from Itelligence and Nikolaj from UCL. We spoke about the impact of education, how am I doing in company, what aspects and tasks I am working on.

# week 11

Adding features- such as lunch sign up for guests and call support button.
Having several meetings with co-workers and optimizing the system.

# week 12

We went to Aalborg, with a purpose of connecting frontend and backend. I had a meeting with niels, Steven and other Itelligence colleagues. We have spoken about sso structure. Niels gave me a talk about message broker, dockers and how the programs architecture work. It was a productive day. We are going to have one more meeting in Aalborg to finish setting up sso-face recognition system.

# week 13

Final week before the exam. Work is done, I am awating logistics and for the meeting date.
